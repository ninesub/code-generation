package com.lfy.generatortest.mapper;

import com.lfy.generatortest.entity.CommAdminArticle;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 李丰翼
 * @since 2020-11-19
 */
public interface CommAdminArticleMapper extends BaseMapper<CommAdminArticle> {

}
