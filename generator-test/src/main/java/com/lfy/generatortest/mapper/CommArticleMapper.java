package com.lfy.generatortest.mapper;

import com.lfy.generatortest.entity.CommArticle;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 文章 Mapper 接口
 * </p>
 *
 * @author 李丰翼
 * @since 2020-11-19
 */
public interface CommArticleMapper extends BaseMapper<CommArticle> {

}
