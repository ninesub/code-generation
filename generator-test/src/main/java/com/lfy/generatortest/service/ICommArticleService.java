package com.lfy.generatortest.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.lfy.generatortest.entity.CommArticle;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import java.util.Map;
/**
 * <p>
 * 文章 服务类
 * </p>
 *
 * @author 李丰翼
 * @since 2020-11-19
 */
public interface ICommArticleService extends IService<CommArticle> {
    IPage<CommArticle> queryAllByPage(int page,int limit,Map<String, Object> queryMap);
    List<CommArticle> queryAll(Map<String, Object> queryMap);
    CommArticle queryOne(Map<String, Object> queryMap);
}
