package com.lfy.generatortest.service.impl;

import com.lfy.generatortest.entity.CommAdminArticle;
import com.lfy.generatortest.mapper.CommAdminArticleMapper;
import com.lfy.generatortest.service.ICommAdminArticleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lfy.generatortest.common.QueryWapperUtils;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;
/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 李丰翼
 * @since 2020-11-19
 */
@Service
public class CommAdminArticleServiceImpl extends ServiceImpl<CommAdminArticleMapper, CommAdminArticle>implements ICommAdminArticleService {
    @Autowired
    private CommAdminArticleMapper commAdminArticleMapper;

    @Override
    public IPage<CommAdminArticle>queryAllByPage(int page,int limit,Map<String,Object>queryMap){
        IPage<CommAdminArticle> commAdminArticleIPage=new Page<>();
        commAdminArticleIPage.setCurrent(page);
        commAdminArticleIPage.setSize(limit);
        QueryWrapper<CommAdminArticle> commAdminArticleQueryWrapper=QueryWapperUtils.createQueryWapper(queryMap,CommAdminArticle.class);
        commAdminArticleIPage= commAdminArticleMapper.selectPage(commAdminArticleIPage, commAdminArticleQueryWrapper);
        return commAdminArticleIPage;
    }
    @Override
    public List<CommAdminArticle> queryAll(Map<String,Object>queryMap){
        return list(QueryWapperUtils.createQueryWapper(queryMap,CommAdminArticle.class));
    }
    @Override
    public CommAdminArticle queryOne(Map<String,Object>queryMap){
        return this.getOne(QueryWapperUtils.createQueryWapper(queryMap,CommAdminArticle.class));
    }
}
