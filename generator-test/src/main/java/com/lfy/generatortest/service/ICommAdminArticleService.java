package com.lfy.generatortest.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.lfy.generatortest.entity.CommAdminArticle;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import java.util.Map;
/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 李丰翼
 * @since 2020-11-19
 */
public interface ICommAdminArticleService extends IService<CommAdminArticle> {
    IPage<CommAdminArticle> queryAllByPage(int page,int limit,Map<String, Object> queryMap);
    List<CommAdminArticle> queryAll(Map<String, Object> queryMap);
    CommAdminArticle queryOne(Map<String, Object> queryMap);
}
