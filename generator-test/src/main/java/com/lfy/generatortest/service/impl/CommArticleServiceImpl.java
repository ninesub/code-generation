package com.lfy.generatortest.service.impl;

import com.lfy.generatortest.entity.CommArticle;
import com.lfy.generatortest.mapper.CommArticleMapper;
import com.lfy.generatortest.service.ICommArticleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lfy.generatortest.common.QueryWapperUtils;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;
/**
 * <p>
 * 文章 服务实现类
 * </p>
 *
 * @author 李丰翼
 * @since 2020-11-19
 */
@Service
public class CommArticleServiceImpl extends ServiceImpl<CommArticleMapper, CommArticle>implements ICommArticleService {
    @Autowired
    private CommArticleMapper commArticleMapper;

    @Override
    public IPage<CommArticle>queryAllByPage(int page,int limit,Map<String,Object>queryMap){
        IPage<CommArticle> commArticleIPage=new Page<>();
        commArticleIPage.setCurrent(page);
        commArticleIPage.setSize(limit);
        QueryWrapper<CommArticle> commArticleQueryWrapper=QueryWapperUtils.createQueryWapper(queryMap,CommArticle.class);
        commArticleIPage= commArticleMapper.selectPage(commArticleIPage, commArticleQueryWrapper);
        return commArticleIPage;
    }
    @Override
    public List<CommArticle> queryAll(Map<String,Object>queryMap){
        return list(QueryWapperUtils.createQueryWapper(queryMap,CommArticle.class));
    }
    @Override
    public CommArticle queryOne(Map<String,Object>queryMap){
        return this.getOne(QueryWapperUtils.createQueryWapper(queryMap,CommArticle.class));
    }
}
