package com.lfy.generatortest.common;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author: 李丰翼
 * @DateTime: 2020/9/6 0006 17:20
 * @Description: 封装公共结果返回
 */
@ApiModel("公共返回模版")

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseDto<T> {
    @ApiModelProperty("状态码")
    private Long status;
    @ApiModelProperty("消息")
    private String message;
    @ApiModelProperty("数据对象")
    private T object;


    /**
     * 成功返回结果
     *
     * @return
     */
    public static ResponseDto success() {
        return new ResponseDto(ResultCode.SUCCESS.getCode()
                , ResultCode.SUCCESS.getMessage(), null);
    }

    /**
     * 成功返回结果
     *
     * @param data
     * @param <T>
     * @return
     */
    public static <T> ResponseDto<T> success(T data) {
        return new ResponseDto<>(ResultCode.SUCCESS.getCode(), ResultCode.SUCCESS.getMessage(), data);
    }

    /**
     * 成功返回结果
     *
     * @param message
     * @param data
     * @param <T>
     * @return
     */
    public static <T> ResponseDto<T> success(String message, T data) {
        return new ResponseDto<>(ResultCode.SUCCESS.getCode(), message, data);
    }

    /**
     * 失败返回结果
     *
     * @param errorCode 错误码
     */
    public static <T> ResponseDto<T> failed(ResultCode errorCode) {
        return new ResponseDto<T>(errorCode.getCode(), errorCode.getMessage(), null);
    }

    /**
     * 失败返回结果
     *
     * @param message 提示信息
     */
    public static <T> ResponseDto<T> failed(String message) {
        return new ResponseDto<T>(ResultCode.FAILED.getCode(), message, null);
    }

    /**
     * 失败返回结果
     */
    public static <T> ResponseDto<T> failed() {
        return failed(ResultCode.FAILED);
    }

    /**
     * 参数验证失败返回结果
     */
    public static <T> ResponseDto<T> validateFailed() {
        return failed(ResultCode.VALIDATE_FAILED);
    }

    /**
     * 参数验证失败返回结果
     *
     * @param message 提示信息
     */
    public static <T> ResponseDto<T> validateFailed(String message) {
        return new ResponseDto<T>(ResultCode.VALIDATE_FAILED.getCode(), message, null);
    }

    /**
     * 未登录返回结果
     */
    public static <T> ResponseDto<T> unauthorized(T data) {
        return new ResponseDto<T>(ResultCode.UNAUTHORIZED.getCode(), ResultCode.UNAUTHORIZED.getMessage(), data);
    }

    /**
     * 未授权返回结果
     */
    public static <T> ResponseDto<T> forbidden(T data) {
        return new ResponseDto<T>(ResultCode.FORBIDDEN.getCode(), ResultCode.FORBIDDEN.getMessage(), data);
    }
}
