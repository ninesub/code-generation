package com.lfy.generatortest.common;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;


import java.util.Map;

/**
 * @author: 李丰翼
 * @since: 2020/11/19
 * @description:
 */
public class QueryWapperUtils {
    public static <T> QueryWrapper<T> createQueryWapper(Map<String,Object> queryMap,Class<T> entityClass){
        QueryWrapper<T> queryWrapper=new QueryWrapper<>();
        //判断是否有查询条件
        if(queryMap!=null){
            queryMap.forEach((k,v)->{
                if(v!=null&&!"".equals(v)){
                    queryWrapper.like(convertJavaToColumn(k),v);
                }
            });
        }
        return queryWrapper;
    }

    /**
     * java属性名转列名
     * @param s java属性名
     * @return 列名
     */
    public static String convertJavaToColumn(String s){
        char[] chars = s.toCharArray();
        StringBuffer res = new StringBuffer("");
        for (int i = 0; i < chars.length; i++) {
            if (Character.isUpperCase(chars[i])){
                res.append("_").append(Character.toLowerCase(chars[i]));
            }
            else{
                res.append(chars[i]);
            }
        }
        return res.toString();

    }

}
