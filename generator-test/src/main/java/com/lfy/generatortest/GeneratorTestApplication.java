package com.lfy.generatortest;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
@MapperScan("com.lfy.generatortest.mapper")
@SpringBootApplication
public class GeneratorTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(GeneratorTestApplication.class, args);
	}

}
