package com.lfy.generatortest.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.LinkedHashMap;
import java.util.stream.Collectors;
import com.lfy.generatortest.entity.CommArticle;
import com.lfy.generatortest.service.ICommArticleService;
import com.lfy.generatortest.common.ResponseDto;
import com.lfy.generatortest.common.ResponsePageDto;

import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.*;
import com.github.xiaoymin.knife4j.annotations.DynamicParameter;
import com.github.xiaoymin.knife4j.annotations.DynamicParameters;
/**
 * <p>
 * 文章 控制器
 * </p>
 *
 * @author 李丰翼
 * @since 2020-11-19
 */
@Api(tags = {"文章 控制器" })
@RestController
@RequestMapping("/commArticle")
public class CommArticleController {
    @Autowired
    private ICommArticleService commArticleService;
    /**
    * 列表
    */
    @ApiOperation("显示按条件查询的结果列表:文章")
    @PostMapping("/list")
    public ResponseDto<List<CommArticle>> list(@RequestBody(required = false) Map<String, Object> queryMap){
        return ResponseDto.success(commArticleService.queryAll(queryMap));
    }

    /**
    * 分页列表
    */
    @ApiOperation(value = "根据给定参数查询，显示分页:文章")
    @DynamicParameters(name = "CommArticleHashMapModel",properties = {
            @DynamicParameter(name = "page", value = "当前页",example ="1", required = true, dataTypeClass = Integer.class),
            @DynamicParameter(name = "limit", value = "每页记录数",example = "10",required = true, dataTypeClass = Integer.class),
            @DynamicParameter(name = "query", value = "查询参数", dataTypeClass = CommArticle.class)
    })
    @PostMapping("/listPage")
    public ResponsePageDto<List<CommArticle>> listPage(@RequestBody Map<String, Object> queryMap){
        int page=(int)queryMap.getOrDefault("page",1);
        int limit=(int)queryMap.getOrDefault("limit",10);
        LinkedHashMap<String,Object> query = (LinkedHashMap) queryMap.get("query");
        IPage<CommArticle> respage= commArticleService.queryAllByPage(page,limit,query);
        return ResponsePageDto.success(respage.getRecords(),respage.getTotal());
    }


    /**
     * 信息
     */
    @ApiOperation("显示按条件查询的单个结果:文章")
    @PostMapping("/info")
    public ResponseDto<CommArticle> info(@RequestBody Map<String, Object> queryMap){
        return ResponseDto.success(commArticleService.queryOne(queryMap));
    }

    /**
     * 保存
     */
    @ApiOperation("保存:文章")
    @PostMapping("/save")
    public ResponseDto save(@RequestBody CommArticle commArticle){
        commArticleService.save(commArticle);
        return ResponseDto.success();
    }

    /**
     * 修改
     */
    @ApiOperation("修改:文章")
    @PostMapping("/update")
    public ResponseDto update(@RequestBody CommArticle commArticle){
        commArticleService.updateById(commArticle);
        return ResponseDto.success();
    }

    /**
     * 删除
     */
    @ApiOperation("多个删除:文章")
    @PostMapping("/delete")
    public ResponseDto delete(@RequestBody Integer[]ids){
        commArticleService.removeByIds(Arrays.asList(ids));
        return ResponseDto.success();
    }

}

