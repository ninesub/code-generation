package com.lfy.generatortest.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.LinkedHashMap;
import java.util.stream.Collectors;
import com.lfy.generatortest.entity.CommAdminArticle;
import com.lfy.generatortest.service.ICommAdminArticleService;
import com.lfy.generatortest.common.ResponseDto;
import com.lfy.generatortest.common.ResponsePageDto;

import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.*;
import com.github.xiaoymin.knife4j.annotations.DynamicParameter;
import com.github.xiaoymin.knife4j.annotations.DynamicParameters;
/**
 * <p>
 *  控制器
 * </p>
 *
 * @author 李丰翼
 * @since 2020-11-19
 */
@Api(tags = {"CommAdminArticle 控制器" })
@RestController
@RequestMapping("/commAdminArticle")
public class CommAdminArticleController {
    @Autowired
    private ICommAdminArticleService commAdminArticleService;
    /**
    * 列表
    */
    @ApiOperation("显示按条件查询的结果列表:CommAdminArticle")
    @PostMapping("/list")
    public ResponseDto<List<CommAdminArticle>> list(@RequestBody(required = false) Map<String, Object> queryMap){
        return ResponseDto.success(commAdminArticleService.queryAll(queryMap));
    }

    /**
    * 分页列表
    */
    @ApiOperation(value = "根据给定参数查询，显示分页:CommAdminArticle")
    @DynamicParameters(name = "CommAdminArticleHashMapModel",properties = {
            @DynamicParameter(name = "page", value = "当前页",example ="1", required = true, dataTypeClass = Integer.class),
            @DynamicParameter(name = "limit", value = "每页记录数",example = "10",required = true, dataTypeClass = Integer.class),
            @DynamicParameter(name = "query", value = "查询参数", dataTypeClass = CommAdminArticle.class)
    })
    @PostMapping("/listPage")
    public ResponsePageDto<List<CommAdminArticle>> listPage(@RequestBody Map<String, Object> queryMap){
        int page=(int)queryMap.getOrDefault("page",1);
        int limit=(int)queryMap.getOrDefault("limit",10);
        LinkedHashMap<String,Object> query = (LinkedHashMap) queryMap.get("query");
        IPage<CommAdminArticle> respage= commAdminArticleService.queryAllByPage(page,limit,query);
        return ResponsePageDto.success(respage.getRecords(),respage.getTotal());
    }


    /**
     * 信息
     */
    @ApiOperation("显示按条件查询的单个结果:CommAdminArticle")
    @PostMapping("/info")
    public ResponseDto<CommAdminArticle> info(@RequestBody Map<String, Object> queryMap){
        return ResponseDto.success(commAdminArticleService.queryOne(queryMap));
    }

    /**
     * 保存
     */
    @ApiOperation("保存:CommAdminArticle")
    @PostMapping("/save")
    public ResponseDto save(@RequestBody CommAdminArticle commAdminArticle){
        commAdminArticleService.save(commAdminArticle);
        return ResponseDto.success();
    }

    /**
     * 修改
     */
    @ApiOperation("修改:CommAdminArticle")
    @PostMapping("/update")
    public ResponseDto update(@RequestBody CommAdminArticle commAdminArticle){
        commAdminArticleService.updateById(commAdminArticle);
        return ResponseDto.success();
    }

    /**
     * 删除
     */
    @ApiOperation("多个删除:CommAdminArticle")
    @PostMapping("/delete")
        public ResponseDto delete(@RequestBody Integer[]ids){
        commAdminArticleService.removeByIds(Arrays.asList(ids));
        return ResponseDto.success();
    }

}

