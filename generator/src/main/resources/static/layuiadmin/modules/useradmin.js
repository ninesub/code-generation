/**

 @Name：layuiAdmin 用户管理 管理员管理 角色管理
 @Author：star1029
 @Site：http://www.layui.com/admin/
 @License：LPPL

 */


layui.define(['table', 'form', 'admin'], function (exports) {
    var $ = layui.$
        , table = layui.table
        , setter = layui.setter
        , form = layui.form
        , admin = layui.admin;
    var token = setter.getToken();
    //病人用户管理
    table.render({
        elem: '#LAY-user-manage'
        , url: '/member/patient/list' //模拟接口
        , method: 'post'
        , contentType: 'application/json'
        , headers: {'Authorization': token}
        , page: true //开启分页
        , toolbar: true //让工具栏左侧显示默认的内置模板
        , totalRow: true
        , loading: true
        , cols: [[
            {type: 'checkbox', fixed: 'left'}
            , {field: 'id', width: 100, title: 'ID', sort: true}
            , {field: 'name', title: '姓名', minWidth: 100}
            , {field: 'gender', title: '性别'}
            , {field: 'age', width: 80, title: '年龄'}
            , {field: 'phone', title: '手机'}
            , {field: 'address', title: '住址'}


        ]]
        , parseData: function (res) { //res 即为原始返回的数据
            return {
                "code": res.status, //解析接口状态
                "msg": res.message, //解析提示文本
                "count": res.object.total, //解析数据长度
                "data": res.object.records //解析数据列表
            };
        }
        , response: {
            statusCode: 200 //规定成功的状态码，默认：0

        }
        , page: true
        , limit: 30
        , height: 'full-220'
    });


    //管理员管理
    table.render({
        elem: '#LAY-user-back-manage'
        , url: '/member/docter/getAll' //模拟接口
        , contentType: 'application/json'
        , method: 'post'
        , headers: {'Authorization': token}
        , page: true //开启分页
        , toolbar: true //让工具栏左侧显示默认的内置模板
        , totalRow: true
        , loading: true
        , cols: [[
            {type: 'checkbox', fixed: 'left'}
            , {field: 'id', width: 80, title: 'ID', hide: true}
            , {field: 'phone', title: '手机'}
            , {field: 'name', title: '登录名'}
            , {field: 'role', title: '角色', hide: true}
            , {field: 'roleName', title: '角色名', sort: true}
            , {field: 'date', title: '加入时间', sort: true}
            , {field: 'status', title: '账号状态', templet: '#buttonTpl', minWidth: 80, align: 'center'}
            // , {title: '操作', width: 150, align: 'center', fixed: 'right', toolbar: '#table-useradmin-admin'}
        ]]


    });


    //角色管理
    table.render({
        elem: '#LAY-user-back-role'
        , url: '/member/role/list' //模拟接口
        , method: "get"
        , headers: {'Authorization': token}
        , toolbar: true //让工具栏左侧显示默认的内置模板
        , totalRow: true
        , loading: true
        , cols: [[
            {type: 'checkbox', fixed: 'left'}
            , {field: 'id', width: 80, title: 'ID', sort: true}
            , {field: 'name', title: '角色名'}
            , {field: 'rights', title: '拥有权限'}
            , {field: 'describes', title: '具体描述'}
            // , {title: '操作', width: 150, align: 'center', fixed: 'right', toolbar: '#table-useradmin-admin'}
        ]]
        , text: '对不起，加载出现异常！'
    })
    ;

    //监听工具条 暂时不用
    /*    table.on('tool(LAY-user-back-role)', function (obj) {
            var data = obj.data;
            if (obj.event === 'del') {
                layer.confirm('确定删除此角色？', function (index) {
                    console.log(data)
                    admin.req({
                        url: '/role/delete'
                        , type: "POST"
                        , contentType: "application/json"
                        , data: JSON.stringify(data)
                        , success: function (result) {
                            if (result.data == 1) {
                                obj.del();
                            } else {
                                alert("删除失败！")
                            }

                        }
                    })
                    layer.close(index);
                });
            } else if (obj.event === 'edit') {
                var tr = $(obj.tr);

                layer.open({
                    type: 2
                    , title: '编辑角色'
                    , content: '../../../views/user/administrators/roleform.html'
                    , area: ['500px', '480px']
                    , btn: ['确定', '取消']
                    , yes: function (index, layero) {
                        var iframeWindow = window['layui-layer-iframe' + index]
                            , submit = layero.find('iframe').contents().find("#LAY-user-role-submit");

                        //监听提交
                        iframeWindow.layui.form.on('submit(LAY-user-role-submit)', function (data) {
                            var field = data.field; //获取提交的字段

                            //提交 Ajax 成功后，静态更新表格中的数据
                            //$.ajax({});
                            table.reload('LAY-user-back-role'); //数据刷新
                            layer.close(index); //关闭弹层
                        });

                        submit.trigger('click');
                    }
                    , success: function (layero, index) {

                    }
                })
            }
        });*/

    exports('useradmin', {})
});