layui.define(['element'], function (exports) {
    var $ = layui.$
        , setter = layui.setter
        , admin = layui.admin;
    window.element = layui.element;

    admin.req({
        url: "/member/menu/list",
        type: "post",
        data: JSON.stringify({"userID": setter.getUser().id}),
        dataType: "json",
        contentType: "application/json",
        async: false,
        success: function (result) {
            var menu_data = result.object;
            $('.leftMenu ul').append(getHtml(JSON.parse(menu_data)));
            window.element.render('nav');
        },
        error: function (result) {
            console.log("请求出错");
        }
    })

    /**
     * 根据json解析动态菜单
     */
    function getHtml(data) {
        var ulHtml = '<ul class="layui-nav layui-nav-tree beg-navbar">';
        for (var i = 0; i < data.length; i++) {
            if (data[i].spread) {
                ulHtml += '<li class="layui-nav-item layui-nav-itemed">';
            } else {
                ulHtml += '<li class="layui-nav-item">';
            }
            if (data[i].children !== undefined && data[i].children !== null && data[i].children.length > 0) {
                ulHtml += '<a href="javascript:;">' + data[i].title;
                ulHtml += '<span class="layui-nav-more"></span>';
                ulHtml += '<i class="' + data[i].icon + '"></i>';
                ulHtml += '</a>';
                ulHtml += '<dl class="layui-nav-child">';
                //二级菜单
                for (var j = 0; j < data[i].children.length; j++) {
                    //是否有孙子节点
                    if (data[i].children[j].children !== undefined && data[i].children[j].children !== null && data[i].children[j].children.length > 0) {
                        ulHtml += '<dd>';
                        ulHtml += '<a href="javascript:;">' + data[i].children[j].title;
                        ulHtml += '<span class="layui-nav-more"></span>';

                        ulHtml += '</a>';
                        //三级菜单
                        ulHtml += '<dl class="layui-nav-child">';
                        var grandsonNodes = data[i].children[j].children;
                        for (var k = 0; k < grandsonNodes.length; k++) {
                            ulHtml += '<dd>';
                            ulHtml += '<a lay-href="' + grandsonNodes[k].href + '">' + grandsonNodes[k].title + '</a>';
                            ulHtml += '</dd>';
                        }
                        ulHtml += '</dl>';
                        ulHtml += '</dd>';
                    } else {
                        ulHtml += '<dd>';
                        ulHtml += '<a lay-href="' + data[i].children[j].href + '">' + data[i].children[j].title;
                        ulHtml += '</a>';
                        ulHtml += '</dd>';
                    }
                    //ulHtml += '<dd title="' + data[i].children[j].title + '">';
                }
                ulHtml += '</dl>';
            } else {
                var dataUrl = (data[i].href !== undefined && data[i].href !== '') ? 'data-url="' + data[i].href + '"' : '';
                //ulHtml += '<a href="javascript:;" ' + dataUrl + '>';
                ulHtml += '<a lay-href="' + data[i].href + '"' + dataUrl + '>';
                if (data[i].icon !== undefined && data[i].icon !== '') {
                    if (data[i].icon.indexOf('fa-') !== -1) {
                        ulHtml += '<i class="' + data[i].icon + '" aria-hidden="true" ></i>';
                    } else {
                        ulHtml += '<i class="' + data[i].icon + '"></i>';
                    }
                }
                ulHtml += '<cite>' + data[i].title + '</cite>';
                ulHtml += '</a>';
            }
            ulHtml += '</li>';
        }
        ulHtml += '</ul>';

        return ulHtml;
    }

    //对外暴露的接口
    exports('menu', {});
});