package com.lfy.generator.constant;

/**
 * @Author: 李丰翼
 * @DateTime: 2020/11/8 0008 17:55
 * @Description:
 */
public class FileConst {
    public static final String LINUX_PATH = "/www/generator/";
    public static final String WIN_PATH = System.getProperty("user.dir")+"\\generator\\";
    public static final String LINUX_COMMON_PATH = LINUX_PATH + "common";
    public static final String WIN_COMMON_PATH = WIN_PATH+"common";
    public static final String LINUX_CONFIG_PATH = LINUX_PATH + "config";
    public static final String WIN_CONFIG_PATH = WIN_PATH+"config";
    public static final String LINUX_POM_PATH = LINUX_PATH + "pom.xml";
    public static final String WIN_POM_PATH = WIN_PATH + "pom.xml";

}
