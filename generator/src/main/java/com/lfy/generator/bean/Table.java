package com.lfy.generator.bean;

import lombok.Data;

/**
 * @Author: 李丰翼
 * @DateTime: 2020/11/7 0007 15:48
 * @Description:
 */
@Data
public class Table {
    private String name;
    private String engine;
    private String comment;
    private String createTime;
}
