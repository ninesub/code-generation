package com.lfy.generator.common;

import io.swagger.annotations.Api;
import lombok.Data;

/**
 * @Author: 李丰翼
 * @DateTime: 2020/11/6 0006 21:56
 * @Description:分页数据封装返回
 */
@Api("分页数据封装返回")
@Data
public class ResponsePageDto<T> extends ResponseDto<T> {
    private long total;

    public ResponsePageDto(Long status, String message, T object, long total) {
        super(status, message, object);
        this.total = total;
    }

    /**
     * 成功返回结果
     *
     * @param data
     * @param <T>
     * @return
     */
    public static <T> ResponsePageDto<T> success(T data, long total) {
        return new ResponsePageDto<>(ResultCode.SUCCESS.getCode(), ResultCode.SUCCESS.getMessage(), data, total);
    }

    /**
     * 失败返回结果
     *
     * @param message 提示信息
     */
    public static <T> ResponsePageDto<T> failed(String message) {
        return new ResponsePageDto<T>(ResultCode.FAILED.getCode(), message, null, 0);
    }
}
