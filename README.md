# 代码生成

#### 介绍
根据数据库表生成后台对表格crud等常用操作操作

#### 软件架构
| 框架         | 版本   | 说明                                                         |
| ------------ | ------ | ------------------------------------------------------------ |
| springBoot   | 2.2.10 | java后台主流框架                                             |
| MyBatis-plus | 3.3.1  | 增强版MyBatis,封装了常用的数据库操作                         |
| knife4j      | 2.0.4  | springfox-swagger的增强UI实现，为Java开发者在使用Swagger的时候，能拥有一份简洁、强大的接口文档体验 |



#### 安装教程

项目演示地址：http://generator.lifengyi.cn/

若项目提供的自定义配置满足要求，则可直接在线生成代码，无需克隆项目(服务器不会保存数据库信息)

1.  克隆项目
2.  直接运行
3.  输入http://localhost:8080

#### 使用说明

1. 进行全局参数设置，自定义生成文件的参数

   ![](./readimage/参数.png)



2. 填写数据库配置：在代码生成页填写数据库信息

   ![image-20201117151211274](./readimage/数据库配置.png)

3. 选择需要生成代码的表格

   ![image-20201117151544670](./readimage/选择表格.png)

4. 生成代码

   ![image-20201117154742245](./readimage/生成代码.png)

5. 导入项目
    1. 将生成的文件拷贝到项目中

    2. 根据下载的pom文件内容添加在项目pom中依赖

       ```xml
       	<dependencies>
       		<dependency>
       			<groupId>org.springframework.boot</groupId>
       			<artifactId>spring-boot-starter</artifactId>
       		</dependency>
       		<dependency>
       			<groupId>org.springframework.boot</groupId>
       			<artifactId>spring-boot-starter-web</artifactId>
       		</dependency>
       		<dependency>
       			<groupId>mysql</groupId>
       			<artifactId>mysql-connector-java</artifactId>
       			<scope>runtime</scope>
       		</dependency>
       		<dependency>
       			<groupId>com.baomidou</groupId>
       			<artifactId>mybatis-plus-boot-starter</artifactId>
       			<version>3.3.1</version>
       		</dependency>
       
       		<!--		工具类-->
       		<dependency>
       			<groupId>cn.hutool</groupId>
       			<artifactId>hutool-all</artifactId>
       			<version>5.4.7</version>
       		</dependency>
       		<dependency>
       			<groupId>commons-io</groupId>
       			<artifactId>commons-io</artifactId>
       			<version>2.5</version>
       		</dependency>
       		<dependency>
       			<groupId>org.projectlombok</groupId>
       			<artifactId>lombok</artifactId>
       			<optional>true</optional>
       		</dependency>
       
       		<!--      knife4j 集成了  swagger-ui-->
       		<dependency>
       			<groupId>com.github.xiaoymin</groupId>
       			<artifactId>knife4j-spring-boot-starter</artifactId>
       			<version>2.0.4</version>
       		</dependency>
       		<dependency>
       			<groupId>org.springframework.boot</groupId>
       			<artifactId>spring-boot-starter-test</artifactId>
       			<scope>test</scope>
       			<exclusions>
       				<exclusion>
       					<groupId>org.junit.vintage</groupId>
       					<artifactId>junit-vintage-engine</artifactId>
       				</exclusion>
       			</exclusions>
       		</dependency>
       	</dependencies>
       ```

       

    3. 修改config包下SwaggerConfig文件中api访问参数为自己的controller包

       ```java
       @Configuration
       @EnableSwagger2
       @EnableKnife4j
       public class SwaggerConfig {
           @Bean
           public Docket createRestApi() {
               return new Docket(DocumentationType.SWAGGER_2)
                       .apiInfo(apiInfo())
                       .select()
                       //改为自己的controller包名      	.apis(RequestHandlerSelectors.basePackage("com.lfy.generatortest.controller"))
                       .paths(PathSelectors.any())
                       .build();
           }
       
           private ApiInfo apiInfo() {
               return new ApiInfoBuilder()
                       .title("swagger-bootstrap-ui RESTful APIs")
                       .description("swagger-bootstrap-ui")
                       .termsOfServiceUrl("http://localhost:8999/")
                       .contact("developer@mail.com")
                       .version("1.0")
                       .build();
           }
       }
       
       ```

       

    4. 在配置文件中添加数据库信息

       ```yml
       #数据库连接信息
       spring:
         datasource:
           driver-class-name: com.mysql.cj.jdbc.Driver
           url: jdbc:mysql://localhost/hospital?serverTimezone=UTC
           username: root
           password: root
       
       mybatis-plus:
         global-config:
           db-config:
             id-type: auto
             field-strategy: not_empty
             table-underline: true
             db-type: mysql
             logic-delete-value: 1 # 逻辑已删除值(默认为 1)
             logic-not-delete-value: 0 # 逻辑未删除值(默认为 0)
       server:
         port: 8081
       ```

       

    5. 启动类中添加mapper包扫描注解 @MapperScan("com.lfy.generatortest.mapper")

       ```java
       @MapperScan("com.lfy.generatortest.mapper")
       @SpringBootApplication
       public class GeneratorTestApplication {
       
       	public static void main(String[] args) {
       		SpringApplication.run(GeneratorTestApplication.class, args);
       	}
       
       }
       ```

       

    6. 启动项目 访问localhost:8080/doc.html在线接口文档并调试

![](./readimage/文档.png)